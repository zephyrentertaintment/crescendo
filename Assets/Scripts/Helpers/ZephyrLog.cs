using UnityEngine;
using System.Collections;
 
namespace Helpers
{
    public class ZephyrLog
    {
        //TODO: Berkay Ugur Senocak: Split enum and class into two seperate classes.
        //TODO: Prepare a method for file logging.
        private string _className;
        private PriorityType _priority;

        public enum PriorityType
        {
            Universal = 0,
            Debug = 5,
            Info = 10,
            Warning = 15,
            Error = 20,
            Disabled = 25
        };

        public ZephyrLog(string className, PriorityType priority)
        {
            _className = className;
            _priority = priority;
        }

        public void ReportingDebug(string message, string prefix)
        {
            if (_priority > PriorityType.Debug) return;

            string displayingMessage = string.Format("[{0} *-{1}-* {2}]", "ReportingDEBUG", message, prefix);
            Debug.Log(displayingMessage);
        }

        public void Info(string message, string prefix)
        {
            if (_priority > PriorityType.Info) return;

            string displayingMessage = string.Format("[{0}] [{1}] [{2}] {3}", "INFO", _className, prefix, message);
            Debug.Log(displayingMessage);
        }

        public void Warning(string message, string prefix)
        {
            if (_priority > PriorityType.Warning) return;

            string displayingMessage = string.Format("[{0} *-{1}-* {2}]", "WARNING", message, prefix);
            Debug.LogWarning(displayingMessage);
        }

        public void Error(string message, string prefix)
        {
            if (_priority > PriorityType.Error) return;

            string displayingMessage = string.Format("[{0} *-{1}-* {2}]", "ERROR", message, prefix);
            Debug.Log(displayingMessage);
        }
    }

}
