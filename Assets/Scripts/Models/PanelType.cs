﻿namespace Models
{
    public enum PanelType
    {
        None = 0,
        MainMenu = 1,
        GameMenu = 2,
        Options = 3,
        Stats = 4,
        SaveLoadMenu = 5
    }
}