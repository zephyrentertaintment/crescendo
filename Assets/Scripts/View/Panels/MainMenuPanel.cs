﻿using Helpers;

namespace View.Panels
{
    public class MainMenuPanel : PanelBase {

        private static readonly ZephyrLog _log = new ZephyrLog(typeof(MainMenuPanel).Name, ZephyrLog.PriorityType.Info);

        protected override void Open()
        {
            base.Open();
        }

        private void OnEnable()
        {

        }

        #region GUI Regions

        public void OnStartGameClicked()
        {
            _log.Info("Start Game Pressed", "OnStartGameClicked");
        }

        public void OnLoadGameClicked()
        {
            //Load a saved data, then open game panel.
        }

        public void OnOptionsClicked()
        {
            //Open options popup
        }

        public void OnGalleryClicked()
        {
            //Open Gallery panel
        }

        public void OnCreditsClicked()
        {
            //Open Credits
        }

        public void OnExitClicked()
        {
            //Close the game
        }

        #endregion
    }
}
