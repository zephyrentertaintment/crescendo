﻿using UnityEngine;

namespace View.Panels
{
    public class PanelBase : MonoBehaviour {

        protected virtual void Open()
        {
            //Do for every panel.
        }

        protected virtual void Close()
        {
            //Do for every panel.
        }

    }
}
