﻿using System.Collections.Generic;
using Controllers;
using Helpers;
using Models;
using UnityEngine;
using View.Panels;

namespace Managers
{
    public class CardLayoutManager : MonoBehaviour
    {

        private readonly ZephyrLog _log = new ZephyrLog(typeof(CardLayoutManager).Name, ZephyrLog.PriorityType.Info);

        [SerializeField]
        private Dictionary<PanelType, PanelBase> _panels;

        private void Awake()
        {

        }
    }
}
